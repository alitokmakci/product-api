<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 10)->create()->each(function($u) {
            $u->products()->saveMany(factory(App\Models\Product::class, rand(1,10))->make());
        });
    }
}

## Proje Detayları
- Projede önyüz tasarımını hızlandırmak amacıyla Bootstrap eklendi. 
- RouteServiceProvider mapApiRoutes() fonksiyonunda prefix düzenlemesi yapıldı.
- Welcome blade sayfasında gerekli düzenlemeler yapıldı ve metotlar ve gerekli anahtar bilgileri detaylıca hazırlandı.
- Her Metot için dönen JSON değerlerinin önizlemeleri eklendi.
- Veritabanını doldurmak amacıyla Database Seeder'lar hazırlandı.
- Api Key Sistemi eklenerek kullanıcılara benzersiz api_key'ler tanımlandı.
- Sistemi kullanmak için önerilen adımlar:
  - 0- "composer install" komutu ile gerekli kütüphanelerin yüklenmesi,
  - 1- Veritabanı hazırlanıp "php artisan migrate --seed komutu" ile veritabanını geçici verilerle doldurulması, 
  - 2- Get Key sayfasından benzersiz bir api key'i alınması ve belirtilen metotların test edilmesi.


@extends('app')

@section('content')
    <div class="container">
        @isset($error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endisset

        @isset($success)
            <div class="alert alert-success" role="alert">
                {{ $success }} <br>
                <p>Your api key is: <strong>{{ $apiKey }}</strong></p>
            </div>
        @endisset
        <form method="POST">
            @csrf
            <div class="form-group">
                <label for="email">Your Email Address:</label>
                <input type="email" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-primary">Get Me A Key!</button>
            </div>
        </form>
    </div>
@endsection

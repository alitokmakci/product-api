@extends('app')

@section('content')
    <div class="container">
        <h1>How It Works?</h1>
        <p class="mb-0 mt-4">Send your all requests to:</p>
        <input type="text" class="form-control" readonly value="/api/v1/product?key=[your-api-key]&...">
        <small class="text-danger">*You can get your key from <a href="/get-key">here</a></small>
        <h1 class="pt-4 mb-4">What methods are we support?</h1>
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <th scope="col">METHOD</th>
                <th scope="col">URL</th>
                <th scope="col">Description</th>
                <th scope="col">Details</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <span class="badge badge-success">GET</span>
                </td>
                <td>/api/v1/product</td>
                <td>List of All Products</td>
                <td><a href="#listing">Go To Details</a></td>
            </tr>
            <tr>
                <td>
                    <span class="badge badge-primary">POST</span>
                </td>
                <td>/api/v1/product</td>
                <td>Add new Product To Our Database</td>
                <td><a href="#adding">Go To Details</a></td>
            </tr>
            <tr>
                <td>
                    <span class="badge badge-secondary">PUT</span>
                </td>
                <td>/api/v1/product</td>
                <td>Update a Specific Product By ID</td>
                <td><a href="#updating">Go To Details</a></td>
            </tr>
            <tr>
                <td>
                    <span class="badge badge-danger">DELETE</span>
                </td>
                <td>/api/v1/product</td>
                <td>Delete One Of Your Products</td>
                <td><a href="#deleting">Go To Details</a></td>
            </tr>
            </tbody>
        </table>
        <h1 class="pt-4 mb-4">Digging Deeper</h1>
        <div id="listing">
            <h5>
                <i class="fas fa-angle-double-right"></i> Listing and Filtering All Products:
            </h5>
            <p class="mb-0">
                First of all you should use: " <strong>/api/v1/product</strong> " address with your
                parameters and your api key.
            </p>
            <small class="text-danger font-weight-bold">
                *Do not forget to use <span class="badge badge-success">GET</span> method.
            </small>
            <h6 class="mt-3">Parameters:</h6>
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th scope="col">Parameter</th>
                    <th scope="col">Type</th>
                    <th scope="col">Default Value</th>
                    <th scope="col">Required</th>
                    <th scope="col">Options</th>
                    <th scope="col">Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>Integer</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>Returns only 1 product. Can't paginate if this parameter is used</td>
                    <td>A valid product ID</td>
                </tr>
                <tr>
                    <td>name</td>
                    <td>String</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>-</td>
                    <td>A valid name</td>
                </tr>
                <tr>
                    <td>price</td>
                    <td>String, Integer, Float</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        <strong>max10</strong>: lower than 10.01₺,<br>
                        <strong>min10</strong>: higher than 9.99₺,<br>
                        <strong>12.50</strong>: equals to 12.50₺,<br>
                        <strong>min10-max100</strong>: from 10₺ to 100₺<br>
                        <small class="text-danger">*You can use any numbers with theese 4 options. Given numbers are example.</small>
                    </td>
                    <td>Price of product or range of prices</td>
                </tr>
                <tr>
                    <td>status</td>
                    <td>Integer</td>
                    <td>1</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        <strong>1</strong>: active,<br>
                        <strong>0</strong>: inactive
                    </td>
                    <td>Status of product, active or inactive</td>
                </tr>
                <tr>
                    <td>created</td>
                    <td>Date</td>
                    <td>-</td>
                    <td><span class="badge badge-success">No</span></td>
                    <td><strong>Example</strong>: 13/12/2020</td>
                    <td>Created Date of Product</td>
                </tr>
                <tr>
                    <td>updated</td>
                    <td>Date</td>
                    <td>-</td>
                    <td><span class="badge badge-success">No</span></td>
                    <td><strong>Example</strong>: 02-08-2020</td>
                    <td>Last Updated date of product</td>
                </tr>
                <tr>
                    <td>mine</td>
                    <td>Integer</td>
                    <td>0</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        <strong>1</strong>: returns product which only added by you, <br>
                        <strong>0</strong>: returns product which only added by you
                    </td>
                    <td>Page number</td>
                </tr>
                <tr>
                    <td>paginate</td>
                    <td>Integer</td>
                    <td>10</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>number must be between <strong>10</strong> and <strong>50</strong></td>
                    <td>Custom pagination settings</td>
                </tr>
                <tr>
                    <td>page</td>
                    <td>Integer</td>
                    <td>1</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td></td>
                    <td>Page number</td>
                </tr>
                </tbody>
            </table>
            <p>Example Usage:</p>
            <input type="text" class="form-control" readonly
                   value="/api/v1/product?paginate=10&name=simple&price=min10-max100&status=1&key=xxx"
            >
            <small>10 products per page, products which has name realted to "simple", and has price between 50 and 100
                and
                has active status; will be listed </small>
            <p class="mt-2">Example Return:</p>
            <pre>
            <code>
                {
                    "products":[
                        {
                            "id":1,
                            "name":"Simple Product",
                            "price":10,
                            "status":1,
                            "created_at":"2020-09-15 02:12:01",
                            "updated_at":"2020-09-15 02:12:01"
                        }
                    ],
                    "totalResults":1,
                    "totalPage":1,
                    "currentPage":"1",
                    "perPage":10
                }
            </code>
            </pre>
        </div>
        <div id="adding">
            <h5>
                <i class="fas fa-angle-double-right"></i> Adding A New Product:
            </h5>
            <p class="mb-0">
                First of all you should use: " <strong>/api/v1/product</strong> " address with your
                parameters and your api key.
            </p>
            <small class="text-danger font-weight-bold">
                *Do not forget to use <span class="badge badge-primary">POST</span>method.
            </small>
            <small class="text-danger font-weight-bold">
                *If you send a valid phone number you will be informed via free SMS.
            </small>
            <h6 class="mt-3">Parameters:</h6>
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th scope="col">Parameter</th>
                    <th scope="col">Type</th>
                    <th scope="col">Default Value</th>
                    <th scope="col">Required</th>
                    <th scope="col">Options</th>
                    <th scope="col">Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>name</td>
                    <td>String</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-danger">Yes</span>
                    </td>
                    <td>-</td>
                    <td>A valid name</td>
                </tr>
                <tr>
                    <td>price</td>
                    <td>Integer, Float</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-danger">Yes</span>
                    </td>
                    <td>-</td>
                    <td>Price of product Example: <strong>9.99</strong></td>
                </tr>
                <tr>
                    <td>status</td>
                    <td>Integer</td>
                    <td>1</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        <strong>1</strong>: active,<br>
                        <strong>0</strong>: inactive
                    </td>
                    <td>Status of product, active or inactive</td>
                </tr>
                <tr>
                    <td>phone</td>
                    <td>String</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        You must to use your country code without "+" sign<br>
                        Example: <strong>905551234567</strong>
                    </td>
                    <td>A Valid Phone Number</td>
                </tr>
                </tbody>
            </table>
            <p class="mt-2">Example Return:</p>
            <pre>
            <code>
                {
                    "product": {
                        "name": "Simple Pen",
                        "price": "0.99",
                        "status": 1,
                        "updated_at": "2020-09-15 09:42:37",
                        "created_at": "2020-09-15 09:42:37",
                        "id": 3
                    }
                }
            </code>
            </pre>
        </div>
        <div id="updating">
            <h5>
                <i class="fas fa-angle-double-right"></i> Updating An Existing Product:
            </h5>
            <p class="mb-0">
                First of all you should use: " <strong>/api/v1/product</strong> " address with your
                parameters and your api key.
            </p>
            <small class="text-danger font-weight-bold">
                *Do not forget to use <span class="badge badge-secondary">PUT</span> method.
            </small>
            <br>
            <small class="text-danger font-weight-bold">
                *You can not update a product that you do not own!
            </small>
            <h6 class="mt-3">Parameters:</h6>
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th scope="col">Parameter</th>
                    <th scope="col">Type</th>
                    <th scope="col">Default Value</th>
                    <th scope="col">Required</th>
                    <th scope="col">Options</th>
                    <th scope="col">Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>Integer</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-danger">Yes</span>
                    </td>
                    <td>-</td>
                    <td>A valid Product ID</td>
                </tr>
                <tr>
                    <td>name</td>
                    <td>String</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>-</td>
                    <td>A valid name</td>
                </tr>
                <tr>
                    <td>price</td>
                    <td>Integer, Float</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>-</td>
                    <td>Price of product Example: <strong>9.99</strong></td>
                </tr>
                <tr>
                    <td>status</td>
                    <td>Integer</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-success">No</span>
                    </td>
                    <td>
                        <strong>1</strong>: active,<br>
                        <strong>0</strong>: inactive
                    </td>
                    <td>Status of product, active or inactive</td>
                </tr>
                </tbody>
            </table>
            <p class="mt-2">Example Return:</p>
            <pre>
            <code>
                {
                    "product": {
                        "name": "Simple Pen",
                        "price": "1.99",
                        "status": 1,
                        "updated_at": "2020-09-15 09:42:37",
                        "created_at": "2020-09-15 10:42:37",
                        "id": 3
                    }
                }
            </code>
            </pre>
        </div>
        <div id="deleting">
            <h5>
                <i class="fas fa-angle-double-right"></i> Updating An Existing Product:
            </h5>
            <p class="mb-0">
                First of all you should use: " <strong>/api/v1/product</strong> " address with your
                parameters and your api key.
            </p>
            <small class="text-danger font-weight-bold">
                *Do not forget to use <span class="badge badge-danger">DELETE</span> method.
            </small>
            <br>
            <small class="text-danger font-weight-bold">
                *You can not delete a product that you do not own!
            </small>
            <h6 class="mt-3">Parameters:</h6>
            <table class="table table-bordered table-sm">
                <thead>
                <tr>
                    <th scope="col">Parameter</th>
                    <th scope="col">Type</th>
                    <th scope="col">Default Value</th>
                    <th scope="col">Required</th>
                    <th scope="col">Options</th>
                    <th scope="col">Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>id</td>
                    <td>Integer</td>
                    <td>-</td>
                    <td>
                        <span class="badge badge-danger">Yes</span>
                    </td>
                    <td>-</td>
                    <td>A valid Product ID</td>
                </tr>
                </tbody>
            </table>
            <p class="mt-2">Example Return:</p>
            <pre>
            <code>
                {
                    "success": "Your Product Deleted Successfully."
                }
            </code>
            </pre>
        </div>
    </div>
@endsection

<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class CheckApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('key')) {

            if (User::where('api_key', $request->input('key'))->exists()) {
                return $next($request);
            }

            return response(['error' => 'Your Api Key is Invalid!'], 403);

        }

        return response(['error' => 'You Need An Valid Api Key To Use Api'], 403);
    }
}

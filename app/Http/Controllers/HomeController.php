<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function getKey(Request $request)
    {
        //check email first:
        if (User::where('email', $request->email)->exists()) {
            return view('getKey')->with('error', 'Your email address already in use');
        } else {
            $apiKey = Str::random(10);
            //control api key to make unique one
            while (User::where('api_key', $apiKey)->exists()) {
                $apiKey = Str::random(10);
            }
            //Create New User
            $user = new User();
            $user->email = $request->email;
            $user->api_key = $apiKey;
            $user->save();
            return view('getKey')->with('success', 'Your Api Key Generated Successfully')->with('apiKey', $apiKey);
        }
    }
}

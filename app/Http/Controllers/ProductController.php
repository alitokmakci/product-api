<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\DestroyProductRequest;
use App\Http\Requests\ListProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use App\Models\User;
use App\Services\TextMessageService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Services\ProductService;

class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    protected $productService;

    /**
     * Register Product Service
     * @param ProductService $productService
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }


    /**
     * Display a listing of the resource.
     *
     * @param ListProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(ListProductRequest $request)
    {
        $user = User::where('api_key', $request->input('key'))->first();

        if ($request->has('id')) {

            return response(['data' => $this->productService->show($request->input('id'))]);
        }

        $products = $this->productService->index($request->only([
            'active',
            'name',
            'created',
            'updated',
            'mine',
            'price',
            'min',
            'max',
            'paginate'
        ]), $user);

        return response($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProductRequest $request)
    {
        $user = User::where('api_key', $request->input('key'))->first();

        $product = $this->productService->create($request->only(['name', 'price', 'active']), $user);

        if ($request->has('phone')) {
            (new TextMessageService())->sendTextMessage($product, $request->input('phone'));
        }

        return response(['data' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request)
    {
        $product = Product::findOrFail($request->input('id'));

        $user = User::where('api_key', $request->input('key'))->first();

        if ($product->owner != $user) {

            return response(['error' => 'You Can Not Update A Product That You Do Not Own.'], 403);
        }

        $product = $this->productService->update($product, $request->only(['name', 'price', 'active']));

        return response(['data' => $product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public
    function destroy(DestroyProductRequest $request)
    {
        $product = Product::findOrFail($request->input('id'));

        $user = User::where('api_key', $request->input('key'))->first();

        if ($product->owner != $user) {

            return response(['error' => 'You Can Not Update A Product That You Do Not Own.'], 405);
        }

        $product->delete();

        return response(['success' => 'Your Product Deleted Successfully.']);
    }
}

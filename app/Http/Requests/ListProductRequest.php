<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'integer',
            'paginate' => 'max:50|min:10|integer',
            'price' => 'numeric',
            'min' => 'numeric',
            'max' => 'numeric',
            'page' => 'integer',
            'active' => 'boolean'
        ];
    }
}

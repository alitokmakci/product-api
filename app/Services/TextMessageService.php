<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Services;


use App\Models\Product;
use Twilio\Rest\Client;

class TextMessageService
{
    public function sendTextMessage(Product $product, $phone)
    {
        $account_sid = 'AC8450b5db3a8484df19e37fe40b31257a';
        $auth_token = '40a1e15fe4b9f63d1ee965b07c4a6c2a';
        $twilio_number = '+15005550006';
        $message = 'Your Product Is Added To Our Database Safely! Product Name: ' . $product->name . ' - Price: ' . $product->price . ' - Status: ' . $product->status;
        $client = new Client($account_sid, $auth_token);
        $client->messages->create(
            '+' . $phone,
            array(
                'from' => $twilio_number,
                'body' => $message
            )
        );
    }
}

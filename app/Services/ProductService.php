<?php

namespace App\Services;

use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;

class ProductService
{

    /**
     * @param int $id
     * @return Product|Product[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show(int $id)
    {
        return Product::findOrFail($id);
    }

    /**
     * @param array $attributes
     * @param User $user
     * @return Product|\Illuminate\Database\Eloquent\Builder
     */
    public function index(array $attributes, User $user)
    {
        // Default values
        $paginate = 10;
        $active = True;

        if (!empty($attributes['active'])) {
            $active = $attributes['active'];
        }

        $products = Product::where('active', $active);


        if (!empty($attributes['name'])) {
            $products->where('name', 'LIKE', '%' . $attributes['name'] . '%');
        }

        if (!empty($attributes['created'])) {
            $products->whereDate('created_at', Carbon::parse($attributes['created']));
        }

        if (!empty($attributes['updated'])) {
            $products->whereDate('updated_at', Carbon::parse($attributes['updated']));
        }

        if (!empty($attributes['mine']) && $attributes['active']) {
            $products->where('creator', $user->id);
        }

        if (!empty($attributes['price'])) {
            $products->where('price', $attributes['price']);
        }

        if (empty($attributes['price']) && !empty($attributes['min'])) {
            $products->where('price', '>=', $attributes['min']);
        }

        if (empty($attributes['price']) && !empty($attributes['max'])) {
            $products->where('price', '<=', $attributes['max']);
        }

        if (!empty($attributes['paginate'])) {
            $paginate = $attributes['paginate'];
        }

        $products->paginate($paginate);

        return $products;
    }

    /**
     * @param array $attributes
     * @param User $user
     * @return Product
     */
    public function create(array $attributes, User $user)
    {
        $active = True;

        if (!empty($attributes['active'])) {
            $active = $attributes['active'];
        }

        $product = new Product();

        $product->name = $attributes['name'];
        $product->price = $attributes['prices'];
        $product->active = $active;
        $product->owner()->associate($user);

        $product->save();

        return $product;
    }

    /**
     * @param Product $product
     * @param array $attributes
     * @return Product
     */
    public function update(Product $product, array $attributes)
    {
        foreach ($attributes as $key => $value) {
            if (!empty($value)) {
                $product->$key = $value;
            }
        }
        $product->save();

        return $product;
    }
}
